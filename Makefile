# vim: ft=make noet ts=4 sts=4 sw=4

SHELL				 = /bin/bash

REP_FILES			:= css/github-markdown.css
REP_FILES			+= js/jquery.min.js
REP_FILES			+= js/chart.min.js
REP_FILES			+= js/moment-with-locales.min.js

.PHONY:				clean all archive

all:				$(REP_FILES)
	$(MAKE) -f MakefileReport
	$(MAKE) -f MakefileSlide

css/github-markdown.css:				github-markdown-css/github-markdown.css
	sed -r \
		-e 's/[^[:space:],]\.markdown-body/ .markdown-body/g' \
		-e 's/\.markdown-body([^[:alnum:]_-]|$$)/body\1/g' \
		github-markdown-css/github-markdown.css \
		> css/github-markdown.css

js/jquery.min.js:	jquery/dist/jquery.min.js
	cp jquery/dist/jquery.min.js js/

.PHONY:				js/chart.min.js
js/chart.min.js:
	curl https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.7/chart.min.js > js/chart.min.js

js/moment-with-locales.min.js:			moment.js/min/moment-with-locales.min.js
	cp moment.js/min/moment-with-locales.min.js js/

archive:			$(REP_FILES)
	$(MAKE) -f MakefileReport archive
	7z a templateReport.zip CHANGELOG.md
	7z d templateReport.zip headfoot/licenses/revealjs.txt

	$(MAKE) -f MakefileSlide archive
	7z a templateSlide.zip CHANGELOG.md

clean:
	$(MAKE) -f MakefileReport clean
	$(MAKE) -f MakefileSlide  clean
	$(RM) $(REP_FILES)
