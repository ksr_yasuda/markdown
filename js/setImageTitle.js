window.addEventListener("load", (event) => {
	document.querySelectorAll("img").forEach(
		(img, imgIndex) => {
			if (img.hasAttribute("alt")){
				if (!img.hasAttribute("title")) {
					img.setAttribute("title", img.getAttribute("alt"));
				}
			} else {
				if (img.hasAttribute("title")) {
					img.setAttribute("alt", img.getAttribute("title"));
				}
			}
		}
	);
});
