window.addEventListener("load", (event) => {
	document.querySelectorAll("a").forEach(
		(a, aIndex) => {
			if (a.hasAttribute("href") && !a.hasAttribute("target")) {
				if (/^\s*(https?|ftp):\/\//.test(a.getAttribute("href"))) {
					a.setAttribute("target", "_blank");
					a.setAttribute("rel", "noopener noreferrer");
				}
			}
		}
	);
});
