@echo off
REM vim: ft=dosbatch et ts=4 sts=4 sw=4

set scriptDir=%~dp0

if "%~1"=="" (
    goto usage
)



set pandocOpt=
REM set pandocOpt=%pandocOpt% -f markdown+pandoc_title_block-ascii_identifiers+hard_line_breaks+lists_without_preceding_blankline-implicit_figures+autolink_bare_uris+multiline_tables+pipe_tables
set pandocOpt=%pandocOpt% -f markdown+pandoc_title_block-ascii_identifiers-hard_line_breaks+lists_without_preceding_blankline-implicit_figures+autolink_bare_uris+multiline_tables+pipe_tables
set pandocOpt=%pandocOpt% -t html5
set pandocOpt=%pandocOpt% --webtex="https://latex.codecogs.com/svg.latex?"
set pandocOpt=%pandocOpt% --eol=lf
set pandocOpt=%pandocOpt% --embed-resources --standalone
set pandocOpt=%pandocOpt% -H "%scriptDir%LICENSE"
set pandocOpt=%pandocOpt% -c "%scriptDir%css\github-markdown.css" -c "%scriptDir%css\github-markdown_patch.css" -c "%scriptDir%css\user.css"
set pandocOpt=%pandocOpt% -H "%scriptDir%headfoot\inHeader.txt"
set pandocOpt=%pandocOpt% -H "%scriptDir%headfoot\licenses\github-markdown-css.txt"
REM set pandocOpt=%pandocOpt% -H "%scriptDir%headfoot\licenses\jquery.txt"
REM set pandocOpt=%pandocOpt% -H "%scriptDir%headfoot\licenses\chartjs.txt"
REM set pandocOpt=%pandocOpt% -H "%scriptDir%headfoot\licenses\momentjs.txt"
REM set pandocOpt=%pandocOpt% -N --number-offset=0
set pandocOpt=%pandocOpt% --toc-depth=3 --toc

set exitStatus=0

:argShift
if not "%~1" == "" (
    REM XXX: Make sure the local variables set immediately,
    REM      enable delayed expantion
    setlocal ENABLEDELAYEDEXPANSION
        REM FIXME:
        REM It does not contain image files.
        REM To contain other resources, workdir is the script dir.
        REM In return for this, image files relative to the source file come to be missing.
        REM Put a Makefile for such files.
        set workDir="%~dp0"
        set dstDir="%~dp1"
        set srcFile="%~nx1"
        set dstFile="%~n1.htm"
        set srcFileFull="%~f1"
        set dstFileFull="%~dp1\%~n1.htm"

        echo !srcFileFull!

        REM XXX: For network drive support, pushd the target file directory
        pushd !workDir!
        pandoc %pandocOpt% !srcFileFull! -o !dstFileFull!
        popd
    endlocal

    if ERRORLEVEL 1 set exitStatus=%ERRORLEVEL%

    shift
    goto argShift
)

exit /b %exitStatus%



:usage
echo Usage: %~0 [MarkdownText]
exit /b 1
