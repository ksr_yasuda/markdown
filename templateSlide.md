% Title
% T. Yasuda
% xxxx-xx-xx (xxx)



# Sample Title

##  Sample Page

foo bar foo bar

<span class="fragment semi-fade-in">foo</span> <span class="fragment semi-fade-in">bar</span> <span class="fragment semi-fade-in">foo</span> <span class="fragment semi-fade-in">bar</span>

<div class="fragment semi-fade-in">
test `code`
*em* **strong**
</div>

<div class="fragment semi-fade-in">
```bash
echo 1 2 3
```
</div>

<aside class="notes">
* `Esc`キーで スライド一覧
* 発表者用ノート は、`S`キーを押下
* 資料印刷する際は、`?print-pdf` をURLに付ける。
test `code`
*em* **strong**
```bash
echo 1 2 3
```
</aside>

## Sample Page

|table|table|
|:----|:----|
|table|table|
|table|table|
|table|table|
|table|table|



# Thank you for listening! {.unnumbered}
