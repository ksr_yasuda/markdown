Change Log
================================================================================

v3.3.7 - 2025-02-12
--------------------------------------------------------------------------------

### Changed

* Update submodules
    - jQuery   : v3.7.0 -> v3.7.1
    - Chart.js : v4.4.3 -> v4.4.7

v3.3.6 - 2024-07-22
--------------------------------------------------------------------------------

### Changed

* Rearrange standalone output options
* Remove reveal.js license note from the report archive

v3.3.5 - 2024-06-12
--------------------------------------------------------------------------------

### Changed

* Set output line feed LF

v3.3.4 - 2024-06-12
--------------------------------------------------------------------------------

### Changed

* Fix `vertical-align` for `<dl>`
    - In some list, the heading item symbol position had been wrong.
* Disable text uppercase transformation

v3.3.3 - 2024-06-05
--------------------------------------------------------------------------------

### Changed

* Tweak Makefile pattern substitution

v3.3.2 - 2024-06-05
--------------------------------------------------------------------------------

### Changed

* Cosmetic changes for Makefiles

* Update submodules
    - Chart.js  : v4.4.1 -> v4.4.3
    - reveal.js : v5.0.5 -> v5.1.0

v3.3.1 - 2024-03-06
--------------------------------------------------------------------------------

### Changed

* Beautify CSS indent

* Update submodules
    - reveal.js : v5.0.4 -> v5.0.5

v3.3.0 - 2024-02-16
--------------------------------------------------------------------------------

### Changed

* Use original github-markdown-css repo and modify in make
    - github-markdown-css : v2.3.0.1 -> v2.3.0

v3.2.2 - 2024-01-17
--------------------------------------------------------------------------------

### Fixed

* md2htm.bat: Rearrange `--standalone` related options

### Changed

* Update submodules
    - Chart.js  : v4.3.3  -> v4.4.1
    - moment.js : v2.29.4 -> v2.30.1
    - reveal.js : v4.5.0  -> v5.0.4

v3.2.1 - 2023-08-07
--------------------------------------------------------------------------------

### Fixed

* Makefile: Fix to call make subprocess via `$(MAKE)` variable
    - Also call `rm` command via `$(RM)` variable

### Changed

* Update submodules
    - Chart.js : v4.3.0 -> v4.3.3

v3.2.0 - 2023-06-27
--------------------------------------------------------------------------------

### Changed

* Remove jQuery dependency from header scripts

v3.1.2 - 2023-06-08
--------------------------------------------------------------------------------

### Fixed

* md2htm.bat: Fix js file load failure

v3.1.1 - 2023-06-08
--------------------------------------------------------------------------------

### Fixed

* md2htm.bat: Fix MathJax reference is left
* md2htm.bat: Add doc tweak scripts loaded
* js: Add `rel="noopener noreferrer"` to external url links opened in `_blank`

v3.1.0 - 2023-06-08
--------------------------------------------------------------------------------

### Removed

* Removed MathJax
    - Use `--webtex` option instead.

v3.0.0 - 2023-06-07
--------------------------------------------------------------------------------

### Changed

* Update submodules
    - jQuery    : v3.6.3 -> v3.7.0
    - Chart.js  : v3.9.1 -> v4.3.0
    - reveal.js : v4.4.0 -> v4.5.0

### Fixed

* Slide: Fix `fragment` css for reveal.js v4
    - reveal.js v4 has changed `fragment` treatment.
      To follow, add `semi-fade-in` class to get ready in semi-transparency.

v2.9.1 - 2023-03-10
--------------------------------------------------------------------------------

### Changed

* CSS: Set table thead valign bottom

v2.9.0 - 2023-03-02
--------------------------------------------------------------------------------

### Changed

* Add CSS for printout

* Update submodules
    - jQuery    : v3.5.1 -> v3.6.3
    - reveal.js : v4.3.1 -> v4.4.0

v2.8.0 - 2022-08-04
--------------------------------------------------------------------------------

### Changed

* Replaced Pandoc `--selfcontained` option with `--embed-resources` and `--standalone`

* Update submodules
    - Chart.js  : v3.8.0  -> v3.9.1
    - MathJax   : v3.2.1  -> v3.2.2
    - moment.js : v2.29.3 -> v2.29.4

v2.7.0 - 2022-06-03
--------------------------------------------------------------------------------

### Changed

* Change ordered list (`<ol>`) list style to decimal

* Update submodules
    - moment.js : v2.29.1 -> v2.29.3
    - Chart.js  : v2.9.3  -> v3.8.0
    - MathJax   : v3.1.2  -> v3.2.1
    - reveal.js : v4.1.0  -> v4.3.1

v2.6.3 - 2020-12-25
--------------------------------------------------------------------------------

### Fixed

* Version info update

v2.6.2 - 2020-12-25
--------------------------------------------------------------------------------

### Changed

* Update submodules
    - moment.js : v2.24.0 -> v2.29.1
    - Chart.js  : v2.8.0  -> v2.9.3
    - MathJax   : v2.7.6  -> v3.1.2
    - jQuery    : v3.4.1  -> v3.5.1
    - reveal.js : v3.8.0  -> v4.1.0

v2.6.1 - 2019-09-02
--------------------------------------------------------------------------------

### Changed

* md2htm.bat: Support network drive

v2.6.0 - 2019-07-08
--------------------------------------------------------------------------------

### Changed

* Update submodules
    - For pandoc v2.7.3, Reveal.js must be updated to v3.8.0

v2.5.1 - 2019-04-04
--------------------------------------------------------------------------------

### Changed

* md2htm.bat: Convert each file into corresponding html

v2.4.4 - 2018-06-13
--------------------------------------------------------------------------------

### Changed

* Made file extensions `.md`

v2.4.3 - 2017-12-25
--------------------------------------------------------------------------------

### Fixed

* Disabled `-auto_identifiers` to avoid bugs on it
* Removed slide lv2 heading CSS for old pandoc bug

v2.4.2 - 2017-12-13
--------------------------------------------------------------------------------

### Changed

* Added +autolink_bare_uris option
* Removed section numbering from the instant script

v2.4.1.3 - 2017-12-11
--------------------------------------------------------------------------------

### Fixed

* Make the instant script work in the target file dir

v2.4.1.2 - 2017-12-11
--------------------------------------------------------------------------------

### Fixed

* Fixed the instant script for pandoc v2

v2.4.1.1 - 2017-12-11
--------------------------------------------------------------------------------

### Fixed

* Fixed releal.js submodule URL

v2.4.1 - 2017-12-08
--------------------------------------------------------------------------------

### Fixed

* Fixed not to use image alt text as caption

v2.4.0 - 2017-12-08
--------------------------------------------------------------------------------

### Changed

* Support pandoc v2

v2.3.1 - 2017-11-09
--------------------------------------------------------------------------------

### Added

* Added this template's version info

v2.3.0 - 2017-11-07
--------------------------------------------------------------------------------

### Added

* Added submodules' license notes and version info

v2.2.0 - 2017-11-07
--------------------------------------------------------------------------------

### Changed

* Cleaned up the makefile

v2.1.0 - 2017-10-20
--------------------------------------------------------------------------------

### Fixed

* Fixed submodule URLs to GitHub's

v2.0.2 - 2017-10-19
--------------------------------------------------------------------------------

### Changed

* Set other libraries as git submodules

v2.0.1 - 2017-10-19
--------------------------------------------------------------------------------

### Changed

* Set libraries as git submodules

v2.0.0 - 2017-10-10
--------------------------------------------------------------------------------

### Added

* Released publicly

<!-- vim: set ft=markdown et ts=4 sts=4 sw=4: -->
