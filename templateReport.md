% Title
% T. Yasuda
% xxxx-xx-xx (xxx)



# 更新履歴

--------------------------------------------------------------------------------
Date					Author			Remarks
--------------------	------------	----------------------------------------
xxxx-xx-xx (xxx)		T. Yasuda		新規作成
--------------------------------------------------------------------------------

<!--
xxxx-xx-xx (xxx)		T. Yasuda		**★~1~:**
-->



# Sample

sample

$$f(x) = ax^2 + bx + c$$
$$\int f(x) dx$$
$$\displaystyle \int_0^{\infty} f(x) dx$$

----

<div style="text-align: right;">
以上
</div>
